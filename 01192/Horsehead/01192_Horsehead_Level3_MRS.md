
# MRS: Available IAS-versions for 01192 (Horsehead)

Last update : 31-01-2024

```
 Current recommended ias-version : v.1.6.0
```

# Overview

| IAS-version   |      Background  Subtraction    |   WCS Correction  with Gaia DR3 |   Outlier  Detection |  Fringe Correction |  Residual Fringe Correction | Additional Fringe Correction |
|:-------------:|:-------------------------------:|:-------------------------------:|:--------------------:|:------------------:|:---------------------------:|:----------------------------:|
|     v1.6.0    |                x                |                 x               |           x          |          x         |                             |               x              |
|     v1.5.1    |                x                |                 x               |           x          |          x         |              x              |                              |
|     v1.5.0    |                x                |                 x               |           x          |          x         |              x              |                              |
|      v1.4     |                x                |                 x               |           x          |          x         |              x              |                              |
|      v1.3     |                                 |                 x               |           x          |          x         |              x              |                              |
|      v1.0     |                x                |                 x               |           x          |          x         |              x              |                              |

# Detailed Description

## Version v1.6.1

- JWST pipeline version: 1.16.0
- CRDS context: jwst_1293.pmap

### Data Reduction Information 

- Background subtracted (pixel to pixel)   
- WCS corrected with Gaia DR3 (using MIRIM parallel observation of F560W in stage3)
- Fringes correction applied (flat)                                          
- Residual fringes correction not applied                                 
- Additional fringes correction applied  
- Outlier detection applied


## Version v1.5.1

- JWST pipeline version: 1.13.4.dev18+g87770e3f9 
- CRDS context: jwst_1188.pmap

### Data Reduction Information 

- Background subtracted (master background)
- Outlier detection applied
- WCS corrected with Gaia DR3 (source detection)
- Fringes corrected (flat)
- Residual fringes corrected
- Master background spectrum 01192_HORSEHEAD_Level3_MRS_chALLA_masterbg1d_IASv1.5.1.fits was created from the original master background 01192_HORSEHEAD_Level3_MRS_chALLA_masterbg1d_IASv1.5.0.fits  using a median filter with a minimal window size of 13 in order to remove the spectral lines while keeping the main structures and fringes.


## Version v1.5.0

- JWST pipeline version: 1.13.4.dev18+g87770e3f9 
- CRDS context: jwst_1188.pmap

### Data Reduction Information 

- Background subtracted (master background)
- Outlier detection applied
- WCS corrected with Gaia DR3 (source detection)
- Fringes corrected (flat)
- Residual fringes corrected


## Version v1.4

- JWST pipeline version: 1.9.5.dev25+g708be419
- CRDS context: jwst_1046.pmap

### Data Reduction Information 

- Background subtracted (master background)
- Outlier detection applied
- WCS corrected with Gaia DR3 (source detection)
- Fringes corrected (flat)
- Residual fringes corrected
- Master background spectrum 01192_HORSEHEAD_Level3_MRS_masterbg1D_IASvmedian13.fits was created from the original master background 01192_HORSEHEAD_Level3_MRS_masterbg1D.fitsn using a median filter with a minimal window size of 13 in order to remove the spectral lines while keeping the main structures and fringes.


## Version v1.3

- JWST pipeline version: 1.9.5.dev1+ga467283b
- CRDS context: jwst_1041.pmap

### Data Reduction Information 

- Background not subtracted
- Outlier detection applied
- WCS corrected with Gaia DR3 (source detection)
- Fringes corrected (flat)
- Residual fringes corrected


## Version v1.0

- JWST pipeline version: 1.9.5.dev1+ga467283b
- CRDS context: jwst_1041.pmap

### Data Reduction Information 

- Background subtracted (master background)
- Outlier detection applied
- WCS corrected with Gaia DR3 (source detection)
- Fringes corrected (flat)
- Residual fringes corrected
