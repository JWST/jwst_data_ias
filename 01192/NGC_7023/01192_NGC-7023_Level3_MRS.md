
# MRS: Available IAS-versions for 01192 (NGC-7023)

```
 Current recommended ias-version : v.1.1.0
```

# Overview

|  IAS-version  |     Background  Subtraction     |   WCS Correction  with Gaia DR3 |   Outlier  Detection |  Fringe Correction |  Residual Fringe Correction |
|:-------------:|:-------------------------------:|:-------------------------------:|:--------------------:|:------------------:|:---------------------------:|
|     v1.1.0    |                x                |                 x               |           x          |          x         |              x              |
|     v1.0.1    |                x                |                 x               |           x          |          x         |              x              |
|     v1.0.0    |                x                |                 x               |           x          |          x         |              x              |


# Detailed Description

## Version v1.1.0

- JWST pipeline version: 1.14.0
- CRDS context: jwst_1238.pmap

### Data Reduction Information 

- Background subtracted (median master background, window_size=13)
- WCS corrected with Gaia DR3 (using MIRIM parallel observation of F560W in stage3)                         
- Fringes corrected (flat)
- Residual fringes corrected
- Outlier detection applied


## Version v1.0.1

- JWST pipeline version: 1.13.4.dev16+g593cef16b 
- CRDS context: jwst_1185.pmap

### Data Reduction Information 

- Background subtracted (master background)
- Outlier detection applied
- WCS corrected with Gaia DR3 (source detection)
- Fringes corrected (flat)
- Residual fringes corrected
- Master background spectrum 01192_NGC-7023_Level3_MRS_masterbg1d_IASv1.0.1.fits was created from the original master background 01192_NGC-7023_Level3_MRS_masterbg1d_IASv1.0.0.fits using a median filter with a minimal window size of 13 in order to remove the spectral lines while keeping the main structures and fringes.


## Version v1.0.0

- JWST pipeline version: 1.13.4.dev16+g593cef16b 
- CRDS context: jwst_1185.pmap

### Data Reduction Information 

- Background subtracted (master background)
- Outlier detection applied
- WCS corrected with Gaia DR3 (source detection)
- Fringes corrected (flat)
- Residual fringes corrected
